import java.util.ArrayList;

/**
 * Created by Gosia on 14/10/2015.
 */
public class Basket extends Client {

    private ArrayList<Product> product;

    public Basket(String name, boolean discount) {
        super(name, discount);
    }
    public void addProduct( String productName, double productPrice, boolean productDiscount, boolean productSpecial)
    {
        product.add( new Product (productName, productPrice, productDiscount, productSpecial));
    }

    public void printBasket()
    {
        for(int i = 0; i < product.size(); i++)
        {
            product.get(i).print();
        }
    }
}
