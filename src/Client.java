/**
 * Created by Gosia on 14/10/2015.
 */
public class Client {
    private String name;
    private boolean discount;

    public Client(String name, boolean discount) {
        this.name = name;
        this.discount = discount;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDiscount() {
        return discount;
    }

    public void setDiscount(boolean discount) {
        this.discount = discount;
    }

}
