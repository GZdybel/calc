/**
 * Created by Gosia on 14/10/2015.
 */
public class Product {
    private double price;
    private String name;
    private boolean discount;
    private boolean special;

    public Product(double price, String name, boolean discount, boolean special) {
        this.price = price;
        this.name = name;
        this.discount = discount;
        this.special = special;
    }

    public Product(String productName, double productPrice, boolean productDiscount, boolean productSpecial) {
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDiscount() {
        return discount;
    }

    public void setDiscount(boolean discount) {
        this.discount = discount;
    }

    public boolean isSpecial() {
        return special;
    }

    public void setSpecial(boolean special) {
        this.special = special;
    }

        public void print () {
        System.out.println(name + "   " + price + " ZL");
        }
}
